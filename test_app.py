import unittest

from app import app

app.testing = True


class UnitTest(unittest.TestCase):
    def test_add(self):
        params = {'a': 3, 'b': 5}
        client = app.test_client()
        response = client.get('/add', query_string = params)
        self.assertEqual(8, int(response.data))


    def test_sub(self):
        params = {'a': 3, 'b': 5}
        client = app.test_client()
        response = client.get('/sub', query_string = params)
        self.assertEqual(-2, int(response.data))


if __name__ == '__main__':
    unittest.main()